<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title ?? 'Belajarin' }}</title>
    @vite('resources/css/app.css')
    <script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js"></script>

    <link rel="icon" href="https://cdn-icons-png.flaticon.com/512/8576/8576501.png" type="image/x-icon"/>
    <link rel="shortcut icon" href="https://cdn-icons-png.flaticon.com/512/8576/8576501.png" type="image/x-icon"/>
</head>

<body>
    <nav class="fixed top-0 inset-x-0 bg-white py-4 z-50">
        <div class="container flex justify-between items-center">
            <!-- ni komen -->
            
            <a href="/" class="text-lg text-zinc-800 font-black">Belajar<span
                    class="text-lg text-blue-600">.in</span></a>

            <div class="flex gap-6 items-center">
                @if (auth()->check())
                    {{-- <a class="text-zinc-600 hover:text-zinc-800 font-medium" href="#">Mata Pelajaran</a>
                    <a class="text-zinc-600 hover:text-zinc-800 font-medium" href="#">Jenjang</a> --}}
                    <form action="{{ route('logout') }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="py-2 font-bold px-6 rounded bg-red-600 text-white">Logout</button>
                    </form>
                @endif
            </div>
        </div>
    </nav>
    {{ $slot }}
    <footer class="py-4 border-t">
        <div class="container">
            <p>&copy; Belajarin 2023</p>
        </div>
    </footer>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    {{ $js ?? '' }}
</body>

</html>
